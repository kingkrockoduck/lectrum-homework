// Node modules
import { merge } from "webpack-merge";

// Common Configuration Object
import getCommonConfig from "./webpack.common";

// Import of Constants
import { DIST_DIR } from "../utils/constants";

// Development Configuration Object
export default () =>
  merge(getCommonConfig(), {
    mode: "development",
    devtool: false,
    resolve: {
      alias: {
        "react-dom": "@hot-loader/react-dom",
      },
    },
    output: {
      filename: "[name].js",
      path: DIST_DIR,
      //   publicPath: "/dist",
    },
    devServer: {
      contentBase: "dist",
    },
  });
