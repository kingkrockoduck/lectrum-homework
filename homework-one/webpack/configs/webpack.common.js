// Node modules
import HtmlWebpackPlugin from "html-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import { ProgressPlugin } from "webpack";

// Import of Constants
import { SRC_DIR, DIST_DIR, TEMPLATE, ASSETS_DIR } from "../utils/constants";

// Progress Plugin Handler
const handler = (percentage, message, ...args) => {
  console.info(percentage, message, ...args);
};

// Export Common Config
export default () => ({
  entry: {
    main: [SRC_DIR],
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: TEMPLATE,
      title: "⭐️⭐️⭐️ Lectrum Homework",
      favicon: `${ASSETS_DIR}/images/favicon.svg`,
    }),
    new CleanWebpackPlugin({
      root: DIST_DIR,
      verbose: true,
    }),
    new ProgressPlugin(handler),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: "babel-loader",
            options: {},
          },
        ],
      },
      {
        test: /\.s[ac]ss$/,
        use: [
          {
            loader: "style-loader",
            options: {},
          },
          {
            loader: "css-loader",
            options: { sourceMap: true },
          },
          {
            loader: "sass-loader",
            options: { sourceMap: true },
          },
        ],
      },
    ],
  },
});
