// React Core
import React from "react";

//Styles
import Styles from "./styles.module.scss";

// Components
import Tag from "../../Elements/Tag";
import CommentsCounter from "../../Elements/CommentsCounter";
import LikesCounter from "../../Elements/LikesCounter";

export default function Article({ article }) {
  // Props From News Component
  const { poster, tags, controls } = article;

  // Mapping Tags
  // Warning! Array's Method Index for Keys is only educational purpose!!!
  const Tags = tags.map((tag, index) => <Tag tag={tag} key={index} />);

  return (
    <section className={Styles.article}>
      <header>
        <div className={Styles.poster}>
          <img src={poster} alt="article image" />
        </div>
        <div className={Styles.tags}>{Tags}</div>
      </header>
      <article>
        <h1>American writer of bad cowboy stories arrived in</h1>
        <p>
          Volunteering to help people in need combined with travelling to
          faraway places is a new{" "}
        </p>
      </article>
      <footer>
        <span>15.07.2017</span>
        <div className={Styles.controls}>
          <CommentsCounter comments={controls.commentsCounter} />
          <LikesCounter likes={controls.likesCounter} />
        </div>
      </footer>
    </section>
  );
}
