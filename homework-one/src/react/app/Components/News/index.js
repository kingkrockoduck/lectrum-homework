// React Core
import React from "react";

// Components
import Article from "../Article";

// Styles
import Styles from "./styles.module.scss";

export default function News() {
  // Data
  const db = [
    {
      poster: "https://miro.medium.com/max/1280/1*N6eYi8bOQ9tyZy8NGWDNKA.png",
      tags: ["literature", "books"],
      controls: {
        commentsCounter: 10,
        likesCounter: 315,
      },
      index: 1234,
    },
    {
      poster: "https://miro.medium.com/max/1280/1*N6eYi8bOQ9tyZy8NGWDNKA.png",
      tags: ["literature", "books"],
      controls: {
        commentsCounter: 10,
        likesCounter: 315,
      },
      index: 4321,
    },
    {
      poster: "https://miro.medium.com/max/1280/1*N6eYi8bOQ9tyZy8NGWDNKA.png",
      tags: ["literature", "books"],
      controls: {
        commentsCounter: 10,
        likesCounter: 315,
      },
      index: 555777,
    },
  ];
  // Mapping Artilce Components
  const Articles = db.map((article) => (
    <Article article={article} key={article.index} />
  ));
  return <div className={Styles.news}>{Articles}</div>;
}
