// React Core
import React from "react";
// Styles
import Styles from "./styles.module.scss";

export default function Tag({ tag }) {
  return (
    <div>
      <span className={Styles.tag}>{tag}</span>
    </div>
  );
}
