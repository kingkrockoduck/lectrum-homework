// React Core
import React from "react";
// Styles
import Styles from "./styles.module.scss";

export default function LikesCounter({ likes }) {
  return <span className={Styles.likesCounter}>{likes}</span>;
}
