// React Core
import React from "react";
// Styles
import Styles from "./styles.module.scss";

export default function CommentsCounter({ comments }) {
  return <span className={Styles.commentsCounter}>{comments}</span>;
}
