export default (api) => {
  // Cache
  api.cache(true);

  return {
    presets: [
      "@babel/react",
      [
        "@babel/env",
        {
          debug: true,
          spec: true,
          loose: false,
          modules: false,
        },
      ],
    ],
    plugins: ["@babel/proposal-class-properties", "react-hot-loader/babel"],
  };
};
